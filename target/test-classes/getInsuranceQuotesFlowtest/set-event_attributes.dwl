{
  "headers": {
    "host": "0.0.0.0:8081",
    "integration": "postgress",
    "userid": "user1"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/quotes/user4",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/quotes/{customerID}",
  "relativePath": "/quotes/user4",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {
    "customerID": "user4"
  },
  "rawRequestUri": "/quotes/user4",
  "rawRequestPath": "/quotes/user4",
  "remoteAddress": "/127.0.0.1:59695",
  "requestPath": "/quotes/user4"
}