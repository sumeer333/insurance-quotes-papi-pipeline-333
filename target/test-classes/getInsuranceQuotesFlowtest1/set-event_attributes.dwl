{
  "headers": {
    "host": "0.0.0.0:8081",
    "integration": "salesforce",
    "userid": "user1"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/quotes/user71",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/quotes/{customerID}",
  "relativePath": "/quotes/user71",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {
    "customerID": "user71"
  },
  "rawRequestUri": "/quotes/user71",
  "rawRequestPath": "/quotes/user71",
  "remoteAddress": "/127.0.0.1:64515",
  "requestPath": "/quotes/user71"
}