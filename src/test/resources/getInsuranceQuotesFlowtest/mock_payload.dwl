{
  "Customer-ID": "user4",
  "Customer-Name": "userName4",
  "Age": 38,
  "Contact": 9376543216,
  "Address": "user4address4",
  "Policy-Details": {
    "Policy-ID": "axa_life_user4_trans412",
    "Policy-Provider": "Axa",
    "Policy-TermPeriod": "24 years",
    "Policy-Premium": "2000/year",
    "Policy-Cover": "50 lakh",
    "Policy-Type": "Life Insurance"
  }
}