%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "Customer-ID": "user71",
  "Customer-Name": "userName71",
  "Age": 28.0,
  "Contact": 9234678465,
  "Address": "user71address71",
  "Policy-Details": {
    "Policy-ID": "life_lic_user71_trans12",
    "Policy-Provider": "LIC",
    "Policy-TermPeriod": "34 years",
    "Policy-Premium": "2300/year",
    "Policy-Cover": "5 crore",
    "Policy-Type": "Life Insurance"
  }
})